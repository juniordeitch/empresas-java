package com.imdb.app.controller;

import com.imdb.app.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseController<T> {

    public Map<String, Object> retorno;

    public ResponseEntity<Map<String, Object>> ok(T entity) {
        retorno = new HashMap<>();

        retorno.put("result", "SUCCESS");
        retorno.put("data", entity);

        return new ResponseEntity<Map<String, Object>>(retorno, HttpStatus.OK);
    }

    public ResponseEntity<Map<String, Object>> ok(List<T> entity) {
        retorno = new HashMap<>();

        retorno.put("result", "SUCCESS");
        retorno.put("data", entity);

        return new ResponseEntity<Map<String, Object>>(retorno, HttpStatus.OK);
    }

    public ResponseEntity<Map<String, Object>> ok(Page<T> entity) {
        retorno = new HashMap<>();

        retorno.put("result", "SUCCESS");
        retorno.put("data", entity);

        return new ResponseEntity<Map<String, Object>>(retorno, HttpStatus.OK);
    }

}
