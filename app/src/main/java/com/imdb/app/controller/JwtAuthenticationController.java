package com.imdb.app.controller;

import com.imdb.app.config.JwtTokenUtil;
import com.imdb.app.dto.JwtRequest;
import com.imdb.app.dto.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/imdb/api")
public class JwtAuthenticationController extends BaseController<JwtResponse> {

	private AuthenticationManager authenticationManager;
	private JwtTokenUtil jwtTokenUtil;
	private UserDetailsService jwtInMemoryUserDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<Map<String, Object>> createAuthenticationToken(@RequestBody JwtRequest jwtRequest) throws Exception {

		authenticate(jwtRequest.getUsername(), jwtRequest.getPassword());
		final UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(jwtRequest.getUsername());
		String token = jwtTokenUtil.generateToken(userDetails);

		return ok(new JwtResponse(token));
	}

	private void authenticate(String idUsuario, String senha) throws Exception {
		if (idUsuario == null || senha == null)
			throw new Exception("Usuario ou senha nao informados!");

		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(idUsuario, senha);

		try {
			authenticationManager.authenticate(usernamePasswordAuthenticationToken);
		} catch (BadCredentialsException e) {
			throw new Exception("Credenciais invalidas!", e);
		}
	}

	@Autowired
	private void setAuthenticationManager(AuthenticationManager authenticationManager){
		this.authenticationManager = authenticationManager;
	}

	@Autowired
	private void setJwtTokenUtil(JwtTokenUtil jwtTokenUtil){
		this.jwtTokenUtil = jwtTokenUtil;
	}

	@Autowired
	private void setUserDetailsService(UserDetailsService userDetailsService){
		this.jwtInMemoryUserDetailsService = userDetailsService;
	}

}
