package com.imdb.app.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ResponseEntity<Map<String, Object>> restException(Exception ex) {
        Map<String, Object> retorno = new HashMap<>();

        retorno.put("result", "ERROR");
        retorno.put("data", ex.getMessage());

        return new ResponseEntity<Map<String, Object>>(retorno, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
