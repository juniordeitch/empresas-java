package com.imdb.app.controller;

import com.imdb.app.entity.Usuario;
import com.imdb.app.service.AtualizarUsuarioService;
import com.imdb.app.service.CriarUsuarioService;
import com.imdb.app.service.ExclusaoUsuarioService;
import com.imdb.app.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/imdb/api/usuario")
public class UsuarioController extends BaseController<Usuario> {

    private UsuarioService usuarioService;
    private CriarUsuarioService criarUsuarioService;
    private AtualizarUsuarioService atualizarUsuarioService;
    private ExclusaoUsuarioService exclusaoUsuarioService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> listAll(
            @PageableDefault(
                    sort = "nome",
                    direction = Sort.Direction.ASC,
                    page = 0,
                    size = 5
            ) Pageable page) {

        return ok(usuarioService.findAll(page));
    }

    @RequestMapping(value = "/criar", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> criar(@RequestBody Usuario usuario) throws Exception {
        Usuario entity = criarUsuarioService.criarUsuario(usuario);
        return ok(entity);
    }

    @RequestMapping(value = "/{id}/atualizar", method = RequestMethod.PUT)
    public ResponseEntity<Map<String, Object>> atualizar(@PathVariable String id, @RequestBody Usuario usuario) throws Exception {
        usuario.setId(id);
        Usuario entity = atualizarUsuarioService.atualizarUsuario(usuario);
        return ok(entity);
    }

    @RequestMapping(value = "/{id}/excluir", method = RequestMethod.DELETE)
    public ResponseEntity<Map<String, Object>> excluir(@PathVariable String id) throws Exception {
        Usuario entity = exclusaoUsuarioService.desativarUsuarioById(id);
        return ok(entity);
    }

    @Autowired
    private void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Autowired
    private void setCriarUsuarioService(CriarUsuarioService criarUsuarioService) {
        this.criarUsuarioService = criarUsuarioService;
    }

    @Autowired
    private void setAtualizarUsuarioService(AtualizarUsuarioService atualizarUsuarioService) {
        this.atualizarUsuarioService = atualizarUsuarioService;
    }

    @Autowired
    private void setExclusaoUsuarioService(ExclusaoUsuarioService exclusaoUsuarioService) {
        this.exclusaoUsuarioService = exclusaoUsuarioService;
    }

}
