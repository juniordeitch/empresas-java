package com.imdb.app.controller;

import com.imdb.app.entity.Filme;
import com.imdb.app.service.FilmeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController()
@RequestMapping("/imdb/api/filme")
public class FilmeController extends BaseController<Filme> {

    private FilmeService filmeService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> listAll(
            @PageableDefault(
                    page = 0,
                    size = 5
            ) Pageable page) {

        return ok(filmeService.findAll(page));
    }

    @Autowired
    private void setFilmeService(FilmeService filmeService){
        this.filmeService = filmeService;
    }

}
