package com.imdb.app.service;

import com.imdb.app.entity.Usuario;
import com.imdb.app.repository.UsuarioRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service("pimba")
public class UsuarioService extends AbstractPersistService<UsuarioRepository, Usuario, String> {

    public Page<Usuario> findAll(Pageable page) {
        return repository.findByAdministradorAndAtivo(Boolean.FALSE, Boolean.TRUE, page);
    }

    public Usuario buscarUsuariosPorId(String id) {
        return repository.buscarUsuariosPorId(id);
    }

}
