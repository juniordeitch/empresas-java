package com.imdb.app.service;

import com.imdb.app.entity.Filme;
import com.imdb.app.repository.FilmeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FilmeService extends AbstractPersistService<FilmeRepository, Filme, Long> {

    public Page<Filme> findAll(Pageable page) {
        return repository.findAll(page);
    }

}
