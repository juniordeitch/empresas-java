package com.imdb.app.service;

import com.imdb.app.entity.Usuario;
import com.imdb.app.repository.UsuarioRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;

@Service
public class ExclusaoUsuarioService extends AbstractPersistService<UsuarioRepository, Usuario, String> {

    private String idUsuario;

    public Usuario desativarUsuarioById(String idUsuario) throws Exception {

        this.idUsuario = idUsuario;

        if (validacoesAntesDeDesativarUsuario()) {
            return inativarUsuario();
        } else {
            throw new Exception("Erro ao excluir usuario");
        }

    }

    private Boolean validacoesAntesDeDesativarUsuario() throws Exception {

        if (validarCamposObrigatorio())
            throw new Exception("Id nao preenchido");

        if (!validaIsIdJaExiste())
            throw new Exception("Nao foi encontrado usuario com esse ID");

        if (validaIsUsuarioEstaInativo())
            throw new Exception("Esse usuario ja esta inativo");

        return Boolean.TRUE;

    }

    private Boolean validarCamposObrigatorio() {
        if (this.idUsuario == null)
            return Boolean.TRUE;

        return Boolean.FALSE;
    }

    private Boolean validaIsIdJaExiste() {
        return repository.existsById(this.idUsuario);
    }

    private Boolean validaIsUsuarioEstaInativo() {
        Usuario usuario = repository.getById(this.idUsuario);

        if (usuario != null && !usuario.getAtivo())
            return Boolean.TRUE;

        return Boolean.FALSE;
    }

    private Usuario inativarUsuario() {
        Usuario usuario = repository.getById(this.idUsuario);

        usuario.setAtivo(Boolean.FALSE);
        usuario.setDataUltimaAlteracao(new Date());

        return repository.save(usuario);
    }

}
