package com.imdb.app.service;

import com.imdb.app.entity.Usuario;
import com.imdb.app.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AtualizarUsuarioService extends AbstractPersistService<UsuarioRepository, Usuario, String> {

    private PasswordEncoder passwordEncoder;

    private Usuario usuario;

    public Usuario atualizarUsuario(Usuario usuario) throws Exception {

        this.usuario = usuario;

        if (validacoesAntesDeAtualizarUsuario()) {
            ajustarParaAtualizar();
            return repository.save(this.usuario);
        } else {
            throw new Exception("Erro ao atualizar usuario");
        }

    }

    public Boolean validacoesAntesDeAtualizarUsuario() throws Exception {

        if (validarSeIdEstaPreenchido())
            throw new Exception("Id nao localizado");

        if (validaIsEmailJaExiste())
            throw new Exception("Esse email ja esta sendo utilizado por outra pessoa");

        return Boolean.TRUE;

    }

    public Boolean validarSeIdEstaPreenchido() {
        if (!(this.usuario == null) || !(this.usuario.getId() == null))
            return Boolean.FALSE;

        return Boolean.TRUE;
    }

    public Boolean validaIsEmailJaExiste() {
        if (this.usuario.getEmail() == null)
            return Boolean.FALSE;

        Usuario usuario = repository.buscarUsuariosPorEmail(this.usuario.getEmail());

        if (usuario == null) {
            return Boolean.FALSE;
        } else {
            Usuario usuarioPorIdAndEmail = repository.buscarUsuariosPorIdAndEmail(this.usuario.getId(), this.usuario.getEmail());

            if (usuarioPorIdAndEmail == null)
                return Boolean.TRUE;

            return Boolean.FALSE;
        }
    }

    public void ajustarParaAtualizar() {
        Usuario usuario = repository.getById(this.usuario.getId());

        if (this.usuario.getEmail() != null)
            usuario.setEmail(this.usuario.getEmail());

        if (this.usuario.getAtivo() != null)
            usuario.setAtivo(this.usuario.getAtivo());

        if (this.usuario.getAdministrador() != null)
            usuario.setAdministrador(this.usuario.getAdministrador());

        if (this.usuario.getSenha() != null)
            usuario.setSenha(passwordEncoder.encode(this.usuario.getSenha()));

        if (this.usuario.getNome() != null)
            usuario.setNome(this.usuario.getNome());

        if (this.usuario.getDataNascimento() != null)
            usuario.setDataNascimento(this.usuario.getDataNascimento());

        if (this.usuario.getPais() != null)
            usuario.setPais(this.usuario.getPais());

        if (this.usuario.getSexo() != null)
            usuario.setSexo(this.usuario.getSexo());

        usuario.setDataUltimaAlteracao(new Date());

        this.usuario = usuario;
    }

    @Autowired
    private void setBcryptEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
