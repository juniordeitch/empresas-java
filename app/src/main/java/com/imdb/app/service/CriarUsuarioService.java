package com.imdb.app.service;

import com.imdb.app.entity.Usuario;
import com.imdb.app.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class CriarUsuarioService extends AbstractPersistService<UsuarioRepository, Usuario, String> {

    private PasswordEncoder passwordEncoder;

    private Usuario usuario;

    public Usuario criarUsuario(Usuario usuario) throws Exception {

        this.usuario = usuario;

        if (validacoesAntesDeCriarNovoUsuario()) {

            this.usuario.setSenha(passwordEncoder.encode(this.usuario.getSenha()));
            return repository.save(this.usuario);

        } else {
            throw new Exception("Erro ao criar usuario");
        }

    }

    public Boolean validacoesAntesDeCriarNovoUsuario() throws Exception {

        if (validarCamposObrigatorio())
            throw new Exception("Campos obrigatorios nao preenchidos");

        if (validaIsEmailJaExiste())
            throw new Exception("Esse email ja esta sendo utilizado por outra pessoa");

        if (validaIsIdJaExiste())
            throw new Exception("Esse ID ja esta sendo utilizado por outra pessoa");

        return Boolean.TRUE;

    }

    public Boolean validarCamposObrigatorio() {

        if (this.usuario == null)
            return Boolean.TRUE;

        if (this.usuario.getId() == null)
            return Boolean.TRUE;

        if (this.usuario.getEmail() == null)
            return Boolean.TRUE;

        if (this.usuario.getNome() == null)
            return Boolean.TRUE;

        if (this.usuario.getSenha() == null)
            return Boolean.TRUE;

        return Boolean.FALSE;

    }

    public Boolean validaIsEmailJaExiste() {
        Usuario usuario = repository.buscarUsuariosPorEmail(this.usuario.getEmail());

        if (usuario == null)
            return Boolean.FALSE;

        return Boolean.TRUE;
    }

    public Boolean validaIsIdJaExiste() {
        return repository.existsById(this.usuario.getId());
    }

    @Autowired
    private void setBcryptEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

}
