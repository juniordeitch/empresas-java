package com.imdb.app.service;

import com.imdb.app.entity.Usuario;
import com.imdb.app.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract class AbstractPersistService<R extends JpaRepository<E, OID>, E, OID> {

    protected R repository;

    public Boolean existsById(OID id) {
         return repository.existsById(id);
    }

    public E save(E entity) {
        return repository.save(entity);
    }

    public E findOneById(OID id) {
        if (existsById(id)) {
            E en = repository.getById(id);
            return en;
        } else {
            return null;
        }
    }

    @Autowired
    public void setRepository(R repository) {
        this.repository = repository;
    }


}
