package com.imdb.app.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Filme implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column
    private Long id;

    @Column
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "resp_cadastro")
    private Usuario responsavelCadastro;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "diretor")
    private DiretorFilme diretorFilme;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "genero")
    private GeneroFilme generoFilme;

    @Column
    private String descricao;

    @Column(name = "data_registro")
    private Date dataRegistro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getResponsavelCadastro() {
        return responsavelCadastro;
    }

    public void setResponsavelCadastro(Usuario responsavelCadastro) {
        this.responsavelCadastro = responsavelCadastro;
    }

    public DiretorFilme getDiretorFilme() {
        return diretorFilme;
    }

    public void setDiretorFilme(DiretorFilme diretorFilme) {
        this.diretorFilme = diretorFilme;
    }

    public GeneroFilme getGeneroFilme() {
        return generoFilme;
    }

    public void setGeneroFilme(GeneroFilme generoFilme) {
        this.generoFilme = generoFilme;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataRegistro() {
        return dataRegistro;
    }

    public void setDataRegistro(Date dataRegistro) {
        this.dataRegistro = dataRegistro;
    }
}
