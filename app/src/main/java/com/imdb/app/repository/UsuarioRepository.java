package com.imdb.app.repository;

import com.imdb.app.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, String> {

    @Query("from Usuario where email = ?1")
    Usuario buscarUsuariosPorEmail(String email);

    @Query("from Usuario where id = ?1")
    Usuario buscarUsuariosPorId(String id);

    @Query("from Usuario where id = ?1 and email = ?2")
    Usuario buscarUsuariosPorIdAndEmail(String id, String email);

    Page<Usuario> findByAdministradorAndAtivo(Boolean administrador, Boolean ativo, Pageable pageable);

}
