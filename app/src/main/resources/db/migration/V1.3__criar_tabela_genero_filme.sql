set schema 'imdb';

create table if not exists imdb.genero_filme
(
	id serial not null
		constraint genero_filme_pk
			primary key,
	nome varchar(40) not null,
	data_registro timestamp default current_timestamp
);



