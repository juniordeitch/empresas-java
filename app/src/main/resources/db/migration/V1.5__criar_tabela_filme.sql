set schema 'imdb';

create table if not exists imdb.filme
(
	id serial not null
		constraint filme_pk
			primary key,
	nome varchar(100) not null,
	resp_cadastro varchar(50) not null
		constraint filme_usuario_id_fk
			references imdb.usuario,
	diretor int
		constraint filme_diretor_filme_id_fk
			references imdb.diretor_filme,
	genero int
		constraint filme_genero_filme_id_fk
			references imdb.genero_filme,
	descricao text,
	data_registro timestamp default current_timestamp
);

