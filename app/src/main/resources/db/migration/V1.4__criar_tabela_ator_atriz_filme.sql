set schema 'imdb';

create table if not exists imdb.ator_atriz_filme
(
	id serial not null
		constraint ator_atriz_filme_pk
			primary key,
	nome varchar(100) not null,
	data_registro timestamp default current_timestamp
);
