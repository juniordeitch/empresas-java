set schema 'imdb';

create table if not exists imdb.diretor_filme
(
	id serial not null
		constraint diretor_filme_pk
			primary key,
	nome varchar(100) not null,
	data_registro timestamp default current_timestamp
);


