set schema 'imdb';

INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste1', 'userTeste1@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 1', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste2', 'userTeste2@hotmail.com', true, false, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 2', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste3', 'userTeste3@hotmail.com', true, false, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 3', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste4', 'userTeste4@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 4', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste5', 'userTeste5@hotmail.com', true, false, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 5', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste6', 'userTeste6@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 6', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste7', 'userTeste7@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 7', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste8', 'userTeste8@hotmail.com', true, false, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 8', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste9', 'userTeste9@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 9', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste10', 'userTeste10@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 10', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste11', 'userTeste11@hotmail.com', false, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 12', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste12', 'userTeste12@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 13', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste13', 'userTeste13@hotmail.com', true, false, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 14', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste14', 'userTeste14@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 15', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste15', 'userTeste15@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 16', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste16', 'userTeste16@hotmail.com', false, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 17', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste17', 'userTeste17@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 18', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste18', 'userTeste18@hotmail.com', false, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 19', '1996-07-05', 'Brasil', 'Masculino');
INSERT INTO imdb.usuario (id, email, ativo, administrador, senha, nome, data_nasc, pais, sexo) VALUES ('userTeste19', 'userTeste19@hotmail.com', true, true, '$2a$10$ZCcLq4ctL9.QvZ3UdTPWgOZU9xn4/MSZg6fufndY6FPVBao.Qb73y', 'user Teste 20', '1996-07-05', 'Brasil', 'Masculino');

insert into imdb.ator_atriz_filme (nome) values ('Nicolas Cage');
insert into imdb.ator_atriz_filme (nome) values ('John Wayne');
insert into imdb.ator_atriz_filme (nome) values ('Bruce Willis');
insert into imdb.ator_atriz_filme (nome) values ('Denzel Washington');
insert into imdb.ator_atriz_filme (nome) values ('Robert De Niro');
insert into imdb.ator_atriz_filme (nome) values ('Julia Roberts');
insert into imdb.ator_atriz_filme (nome) values ('Adam Sandler');
insert into imdb.ator_atriz_filme (nome) values ('Brad Pitt');

insert into imdb.diretor_filme (nome) values ('Bernardo Bertolucci');
insert into imdb.diretor_filme (nome) values ('Francis Ford Coppola');
insert into imdb.diretor_filme (nome) values ('Woody Allen');
insert into imdb.diretor_filme (nome) values ('Tim Burton');
insert into imdb.diretor_filme (nome) values ('Alfred Hitchcock');
insert into imdb.diretor_filme (nome) values ('Martin Scorsese');
insert into imdb.diretor_filme (nome) values ('Quentin Tarantino');
insert into imdb.diretor_filme (nome) values ('Jean-Luc Godard');

insert into imdb.genero_filme (nome) values ('Ação');
insert into imdb.genero_filme (nome) values ('Comédia romântica');
insert into imdb.genero_filme (nome) values ('Drama');
insert into imdb.genero_filme (nome) values ('Histórico');
insert into imdb.genero_filme (nome) values ('Comédia');
insert into imdb.genero_filme (nome) values ('Aventura');

insert into imdb.filme (nome, resp_cadastro, diretor, genero, descricao) values ('Cidadão Kane', 'userTeste4', 1, 2, 'É só um teste');
insert into imdb.filme (nome, resp_cadastro, diretor, genero, descricao) values ('O Poderoso Chefão', 'userTeste1', 2, 1, 'É só um teste');
insert into imdb.filme (nome, resp_cadastro, diretor, genero, descricao) values ('Operação Sombra - Jack Ryan', 'userTeste6', 2, 2, 'É só um teste');
insert into imdb.filme (nome, resp_cadastro, diretor, genero, descricao) values ('O Procurado', 'userTeste4', 4, 1, 'É só um teste');
insert into imdb.filme (nome, resp_cadastro, diretor, genero, descricao) values ('Missão: Impossível', 'userTeste10', 5, 5, 'É só um teste');
