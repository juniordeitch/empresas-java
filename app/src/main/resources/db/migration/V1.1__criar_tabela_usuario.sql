set schema 'imdb';

create table if not exists imdb.usuario
(
	id varchar(50) not null
		constraint usuario_pk
			primary key,
	email varchar(100) not null,
	ativo boolean default false not null,
	administrador boolean default false not null,
	senha varchar(300) not null,
	nome varchar(150) not null,
	data_nasc date,
	pais varchar(40),
	sexo varchar(9),
	data_registro timestamp default current_timestamp,
	data_ultima_alteracao timestamp default current_timestamp
);

create unique index if not exists usuario_email_uindex
	on imdb.usuario (email);


